import React from "react";
import { _classes } from '../utilities/helpers';
import styles from '../styles/modules/card.module.scss';
import Image from 'next/image';
import Link from 'next/link';

const cl = _classes(styles);

const DEFAULT_CONTENT = `<p>
  Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ea et
  corporis architecto reprehenderit necessitatibus sapiente cum maxime
  quas, neque reiciendis accusamus molestias, similique quis provident
  impedit? Debitis saepe at ea. Saepe cupiditate natus quos autem
  delectus numquam vero quidem blanditiis mollitia nulla ipsam, quis
  aliquid quibusdam porro illum incidunt fugiat harum officiis?
  Tenetur quos, magni veritatis at ex quasi voluptatibus.
</p> <p>
Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ea et
corporis architecto reprehenderit necessitatibus sapiente cum maxime
quas, neque reiciendis accusamus molestias, similique quis provident
impedit? Debitis saepe at ea. Saepe cupiditate natus quos autem
delectus numquam vero quidem blanditiis mollitia nulla ipsam, quis
aliquid quibusdam porro illum incidunt fugiat harum officiis?
Tenetur quos, magni veritatis at ex quasi voluptatibus.
</p>`;

const Card = ({
  title,
  imageUrl= "https://s3.amazonaws.com/smashatxsite/smash_nov2021_janeyun_jhk00502sized_900x675-1636130750421.jpg",
  content = DEFAULT_CONTENT,
  buttonText,
  path,
  className,
  arrowIcon,
}) => {
  return (
    <div className={`${cl('card')} ${className ?? ''}`}>
      <div className={cl('imageWrapper')}>
        <Image
          src={imageUrl}
          alt={title.split(' ')?.[0] || 'card image' }
          width={100}
          height={100}
        />
      </div>
      <div className={cl('contentWrapper')}>
          <h2>{title}</h2>
          <div
            className={cl("content")}
            dangerouslySetInnerHTML={{ __html: content  }}
          />
          <Link href={path ?? "/"} className={cl("link")}>
              {buttonText}
              {arrowIcon}
          </Link>
      </div>
    </div>
  );
};

export default Card;
