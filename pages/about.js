import { useState, useEffect } from 'react';
import { _classes, apiCall } from '../utilities/helpers';
import Image from 'next/image';
import Intro from '../components/Intro';
import styles from '../styles/pages/About.module.scss';
import { toast } from "react-toastify";
import Card from "../components/card";
import { IoArrowForwardSharp } from "react-icons/io5";

const cl = _classes(styles);

function AboutUs() {
  const [pageData, setPageData] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const getPageData = async () => {
    try {
      setIsLoading(true);
      const { data } = await apiCall("/db.json");
      setPageData(data?.items?.[1]);
    } catch (error) {
      toast.error(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getPageData();
  }, []);
  if (isLoading) {
    return <p>Loading....</p>;
  }
  if (!pageData) {
    return <p>No Data</p>;
  }

  return (
    <div className={cl("_")}>
      <div className={cl("hero")}>
        <Image
          src={pageData.fieldgroup1[0].image1}
          alt=""
          width={1300}
          height={850}
        />
      </div>

      <div id="about">
        <Intro
          title={pageData.h2 || "This is the intro title"}
          meme={pageData.h3 || "Meme Text Here"}
          content={pageData.blurb1 || "This is the intro content"}
          cta={pageData.buttonlink1}
        />
      </div>
      <div className={cl("cardWrapper")}>
        <Card className={'odd'} title={'Plan Your Event'} buttonText="REQUEST YOUR INFORMATION"
           arrowIcon={<IoArrowForwardSharp size={20} className={cl('arrow-up')} />}/>
        <Card className={'even'} title={'Eat. Drink'} buttonText="DOWNLOAD MENU" 
          arrowIcon={<IoArrowForwardSharp size={20} className={cl('arrow-down')} />}/>
      </div>
    </div>
  );
}

export default AboutUs;
