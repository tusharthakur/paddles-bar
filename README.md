This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Installation

Install with node 14.20.1

```bash
yarn
```

## Getting the project started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## GENERAL EXPECTATIONS

1. Show git knowledge by branching and merging, appropriate commit messages, etc.
2. Leave no unused vars or other comments/console logs, etc.
3. Explain your thought process for any decisions you make.
4. Include any additional comments or feedback you would like to provide.

## PART 1

1. Fix any errors that are occurring within the project. This includes any images that are not displayed correctly, any components not displaying, any errors running the project, etc.
2. Get the data properly hooked up. You will find a db.json file in the root of the project. You will need to hook up the data to the page.
3.
4. Explain what components would be needed for the design. What are some considerations we should make/what are some potential issues we might run into? Think Mobile First. Add these to the notes section below.

### PART 1 Notes
For the first part of fixing the errors, I faced issues with db.json where the JSON api endpoint was not working, which I then corrected.The images were also not loading due to incorrect access from the object, which has been fixed now. I adjusted the images on both the homepage and the about page using the image cover property and setting their width to 100%. Lastly, the highlighter on the homepage, which includes an arrow and circle, was not responsive on mobile screens, and this has also been addressed.

## PART 2

1. For part 2, create this section on the about page. Both mobile and desktop comps have been provided. We need pixel perfect replication of the design.

### PART 2 Notes
For 2nd part, firstly I fixed the second section intro section. Padding and images styling need to be fixed. And for the missing part I had created a component named Card component folder. That component is reusable which will automatically take it place.

## OPTIONAL - The following sections are optional

FEEDBACK - We would love to hear your feedback. Your answers will not be counted towards your assessment.

## How long did this assessment take to complete?

## Were any instructions unclear?

## Any additional comments you would like to provide can be added here :
