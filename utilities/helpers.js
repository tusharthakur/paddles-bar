import { BASE_URL } from "./constant";

export async function apiCall(url, method = "GET", data = null) {
  const payload = {
    method: method,
    headers: {
      "Content-Type": "application/json",
    },
  };

  if (data) {
    payload.body = JSON.stringify(data);
  }

  try {
    const response = await fetch(`${BASE_URL}${url}`, payload);

    if (!response.ok) {
      return {
        error: {
          message: `Error: ${response.status}`,
        },
      };
    }
    const data = await response.json();
    return { data };
  } catch (error) {
    return { error: { message: error.message } };
  }
}

export const parse = (json, fallback = false) => {
  try {
    if (json === null || json === '') {
      return fallback;
    }

    return JSON.parse(json) || fallback;
  } catch (e) {
    console.error(e);
    return fallback;
  }
};

// abbreviate class name with a prefix
export const _classes = (styles) => (name) => {
  if (typeof name === 'string') {
    return styles[name] || name || '';
  }

  if (Array.isArray(name)) {
    return name.map((n) => styles[n] || n || '').join(' ');
  }

  return '';
};
